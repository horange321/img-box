# png工具箱 -- beta version

### 介绍
常用图片处理的合集工具箱

### 使用
1. 当需操作整个文件夹时，请选择文件夹中的任意一张图片

### 安装教程
1. 初始化 & 安装库：`make init`
2. 运行：`make run`
3. 生成可执行文件：`make build`

### 特别鸣谢
主程序图标：[上海数慧DIST](http://www.dist.com.cn/) （从 [阿里巴巴矢量图标库](https://www.iconfont.cn/) 下载），由Horange修改\
压缩图标：噷濲（从 [阿里巴巴矢量图标库](https://www.iconfont.cn/) 下载），由Horange修改\
旋转图标：请叫我过儿过过过（从 [阿里巴巴矢量图标库](https://www.iconfont.cn/) 下载），由Horange修改\
缩小图标：十早Hanc（从 [阿里巴巴矢量图标库](https://www.iconfont.cn/) 下载），由Horange修改\
内涵图图标：嘉兴麦云科技（从 [阿里巴巴矢量图标库](https://www.iconfont.cn/) 下载），由Horange修改\

# 注意：内涵图很不稳定，故不推荐使用